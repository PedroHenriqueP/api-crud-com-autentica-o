const express = require ('express');
const mongoose = require ('mongoose');
const requireDir = require('require-dir');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

mongoose.connect('mongodb://localhost/meubanco', { useNewUrlParser: true, 
useCreateIndex: true, useUnifiedTopology: true }).then(() => {
    console.log("MongoDB conectado...");
  }).catch((err) => {
    console.log('Houve um erro ao se conectar ao banco: ' + err);
  });
mongoose.Promise = global.Promise;
requireDir('./models');

app.use('/api', require('./routes'));

require('./controllers/authController')(app);
require('./controllers/projectController')(app);

app.listen(3000);